# Nutty GridSystem

A simple flexbox-based grid system designed to be simple and highly configurable for users.

## Getting started

### instalation
In order to use Nutty gridsystem in your html, download and link css/gridsystem.css into your HTML.

## Building from source.

### Prequisites
The only requirement  to build Nutty gridsystem is a [SASS/SCSS](http://www.sass-lang.com/) compiler.

### Configuration
You can configure different features of Nutty GridSystem by modifying the scss/_variables.scss file.

### Building

To build just compile the scss/gridsystem.scss file and enjoy!  

## Author
Alfredo Giscombe ([Algis-kun](https://bitbucket.org/Algis-kun/))

## Acknoledgement

[Responsive Grid System](http://www.responsivegridsystem.com/) - Inspiration by witch this project was born. 